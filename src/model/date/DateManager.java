package model.date;

/**
 * 
 * interface DataManager.
 *
 */
public interface DateManager {

    /**
     *
     */
    void setUpdateDate();

    /**
     * 
     * @return the update date of character
     */
    double getTimeToMod();
}
