package view;

/**
 * 
 */
public enum Category {
    /**
     * 
     */
    Happiness,
    /**
     * 
     */
    Hungry,
    /**
     * 
     */
    Health,
    /**
     * 
     */
    Cleanness;
}
