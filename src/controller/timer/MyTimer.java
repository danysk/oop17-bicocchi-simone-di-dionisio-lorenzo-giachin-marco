package controller.timer;

/**
 * Timer Interface.
 */
public interface MyTimer {
    /**
     * Start the timer.
     */
    void start();

    /**
     * Cancel timer.
     */
    void stop();

}
